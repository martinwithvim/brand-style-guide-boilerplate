---
layout: page
title: Colours
permalink: /colours/
cover: logo.jpg
extract: Lorem ipsum dolor sit amet, nec saepe iudico labores ne, everti causae sapientem his ut. Sint consectetuer in mel, saepe mediocrem ad has. Id cum eius accommodare. Pro cu illum invenire eleifend, no sea ridens regione gloriatur, et omnis aliquip est. Ad vulputate temporibus disputando ius
---

#### primary colour pallette
	
- neon pantones
- RGB/hex
- CMYK

#### secondary colour pallete

##### Character colours
- RGB/hex
- CMYK

##### Other colours
- RGB/hex
- CMYK