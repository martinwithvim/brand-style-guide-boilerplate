---
layout: page
title: Website Patterns
permalink: /website/
cover: logo.jpg
extract: Lorem ipsum dolor sit amet, nec saepe iudico labores ne, everti causae sapientem his ut. Sint consectetuer in mel, saepe mediocrem ad has. Id cum eius accommodare. Pro cu illum invenire eleifend, no sea ridens regione gloriatur, et omnis aliquip est. Ad vulputate temporibus disputando ius
---

#### Facebook

- avatar
- cover image
- example posts
- hashtags

#### Twitter

- avatar
- cover image
- example posts
- hashtags

#### Instagram

- avatar
- example posts
- hashtags
