# A Brand Style Guide with Vim!

Hello, this is my boilerplate for brand style guides using [Jekyll](http://jekyllrb.com/) to power Liquid templating and partials, and [Gulp](http://gulpjs.com/) to process Sass from the [fffunction sassaparilla](http://http://sass.fffunction.co/) boilerplate.

## Gulp

Lives in the subfolder /_gulp

I have .gitignore-d /_gulp/node_modules but the required devDependencies are as follows (these are also in the package.json so an *$ npm install* will do it too.)

- gulp
- gulp-sass
- gulp-css-globbing
- gulp-minify-css

## Disclaimer

This is very much a work in progress, so it might break lots. You have been warned!

Cheers!

[Martin Coote, Design with Vim](mailto:styleguide@withvim.co.uk)