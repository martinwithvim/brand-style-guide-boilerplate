/**
 * @file project setup functions.
 */

// Create our only global object with the project namespace
var ui = ui || {};

/*=====================================
	Setup small screen menu
*/

ui.setupSSMenu = function()
{
	$('#ui-header-toggle').on('click', function(e) {
		$('#ui-navigation-primary').toggleClass('ui-navigation--open');
		$(this).toggleClass('ui-header__toggle--open');
	});
};


/**
 * Our project namespace.
 */

ui.init = function() 
{

	/* Globally cache the jQuery reference to the document to avoid repeated lookups.
	* This should be used as the base of all document-wide
	* searches where a closer context isn't available e.g.
	* var $someElement = $.root.find(someSelector);
	*/
	$.root = $(document.body);

	/*=====================================
	Call modularised setup functions
	*/
	
	ui.setupSSMenu();
    
};



/* Call the project init function
 * This syntax is equivalant to calling $(document).ready();
 */
$(function() {ui.init();});



