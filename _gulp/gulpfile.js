var gulp = require('gulp');
var minifyCSS = require('gulp-minify-css');
var sass = require('gulp-sass');
var cssGlobbing = require('gulp-css-globbing');

gulp.task('css', function () {
    gulp.src('../assets/css/*.scss')
        .pipe(sass({includePaths: ['./']}))
        .pipe(cssGlobbing())
        .pipe(gulp.dest('../css/'));
});

gulp.task('watch', function () {
    gulp.watch(['../assets/css/**/*.scss'], ['css']);
});

gulp.task('default', ['css', 'watch']);